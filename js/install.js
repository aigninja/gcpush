(function (global) {
    var eventNameSpace = "PLAY2:PN:";
    global.gcPush = {};

    (function () {
        var onSuccess = null,
            onFailure = null;

        global.gcPush.getDeviceRegistrationId = function getDeviceRegistrationId(cbSuccess, cbFail, opts) {
            logger.log("Calling getDeviceRegistrationId with arguments: " + JSON.stringify(opts));
            NATIVE.plugins.sendEvent("PushNotificationManager", "getDeviceRegistrationId", typeof opts === "object" ? JSON.stringify(opts) : "{}");
            if (typeof (cbSuccess) === "function") {
                onSuccess = cbSuccess;
            }
            if (typeof (cbFail) === "function") {
                onFailure = cbFail;
            }
        };

        NATIVE.events.registerHandler(eventNameSpace + 'DEVICE_REGISTRATION', function (response) {
            logger.log("In response handler, response: " + JSON.stringify(response));
            if (response.errorMessage) {
                onFailure && onFailure(response.errorMessage);
            } else {
                onSuccess && onSuccess(null, response.data);
            }
        });

    }());

    global.gcPush.registerPNMessageHandler = function registerPNMessageHandler(cbSuccess, cbFail) {
        NATIVE.events.registerHandler(eventNameSpace + 'MESSAGE', function (response) {
            logger.log("In response handler, response: " + JSON.stringify(response));
            if (response.errorMessage) {
                cbFail && cbFail(response.errorMessage);
            } else {
                cbSuccess && cbSuccess(null, response.data);
            }
        });
    };
}(GLOBAL));
