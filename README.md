# Game Closure Devkit Plugin: gcpush

## Usage

At the top of your game's `src/Application.js`:

~~~
import gcpush;
~~~

Now you can use `gcPush.getDeviceRegistrationId(cbSuccess, cbFail)` API to get device registration ID.

And just register a response handler for push notifications as `gcPush.registerPNMessageHandler(cbSuccess, cbFail);`.

The success handler `cbSuccess` will be called every time a push notification is received with first argument as `error` and second as `message`.


For IOS refer https://github.com/presentcreative/pushnotifications
