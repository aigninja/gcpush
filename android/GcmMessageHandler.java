package com.tealeaf.plugin.plugins;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.tealeaf.EventQueue;
import com.tealeaf.logger;


public class GcmMessageHandler extends IntentService {

    String message;
    private String eventNameSpace = "PLAY2:PN:";

    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        message = extras.getString("data");
        EventQueue.pushEvent(new ResponseEvent(null, message));
        logger.log("GCM PN message received: "+ message);
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public class ResponseEvent extends com.tealeaf.event.Event {
        String errorMessage;
        String data;

        public ResponseEvent() {
            super(eventNameSpace + "MESSAGE");
        }

        public ResponseEvent(String errorMessage, String data) {
            super(eventNameSpace + "MESSAGE");
            this.errorMessage = errorMessage;
            this.data = data;
        }
    }
}
