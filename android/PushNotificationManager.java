package com.tealeaf.plugin.plugins;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.AsyncTask;

import com.tealeaf.EventQueue;
import com.tealeaf.logger;
import com.tealeaf.plugin.IPlugin;
import com.google.android.gms.gcm.GoogleCloudMessaging;


public class PushNotificationManager extends Activity implements IPlugin {

    private Activity _activity;
    private Context _ctx;
    private String eventNameSpace = "PLAY2:PN:";

    GoogleCloudMessaging gcm;
    //FIXME: Hardcoded need to make this configurable
    String PROJECT_NUMBER = "1082369880982";

    public PushNotificationManager() {
    }

    public void onCreateApplication(Context applicationContext) {
        logger.log("in onCreateApplication method..");
        _ctx = applicationContext;
    }

    public void onCreate(Activity activity, Bundle savedInstanceState) {
        logger.log("in onCreate method..");
        _activity = activity;
    }

    public void onResume() {
        logger.log("in onResume method..");
    }

    public void onStart() {
        logger.log("in onStart method..");
    }

    public void onPause() {
    }

    public void onStop() {
        logger.log("in onStop method..");
    }

    public void onDestroy() {
    }

    public void onNewIntent(Intent intent) {
    }

    public void setInstallReferrer(String referrer) {
    }

    public void onActivityResult(Integer request, Integer result, Intent data) {
    }

    private void showGPSDisabledAlertToUser() {
    }

    public void logError(String error) {
    }

    public void onBackPressed() {
    }

    public class ResponseEvent extends com.tealeaf.event.Event {
        String errorMessage;
        String data;

        public ResponseEvent() {
            super(eventNameSpace + "DEVICE_REGISTRATION");
        }

        public ResponseEvent(String errorMessage, String data) {
            super(eventNameSpace + "DEVICE_REGISTRATION");
            this.errorMessage = errorMessage;
            this.data = data;
        }
    }

    public void getDeviceRegistrationId(String input){
        logger.log("Getting device registration id");
        String registrationId = "";
        try {
            if (gcm == null) {
                logger.log("Getting gcm instance");
                gcm = GoogleCloudMessaging.getInstance(_ctx);
            }
            logger.log("Registering device");
            registrationId = gcm.register(PROJECT_NUMBER);
            logger.log("Device registered successfully with registratio id :",  registrationId);
            EventQueue.pushEvent(new ResponseEvent(null, registrationId));
        } catch (Exception e) {
            EventQueue.pushEvent(new ResponseEvent("Exception:" + e.getMessage() +" [[STACK]] :"+stackTraceToString(e), ""));
            logger.log(e);
            return;
        }
    }

    private static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}

